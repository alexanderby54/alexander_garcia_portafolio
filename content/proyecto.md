---
title: Proyectos
---
## Estos son los proyectos en los que he trabajado.

### GeoRecycle
    
> Noviembre 2020 / Marzo 2021

Este proyecto es una app de android. El objetivo de GeoRecycle, es que la sociedad sea más consciente de lo que contaminamos a nuestro planeta. Para solucionar esto hemos creado una app, que nos permite tomar fotografías de todos los malos hábitos que hacemos al planeta.

Con el objetivo de solucionar estos malos hábitos hemos creado un mapa que nos permite saber exactamente donde se han hecho. Además de todo esto, en la aplicación podemos encontrar consejos con el fin de mejorar nuestro estilo de vida, en la sección de productos tenemos todos los objetos con su respectivo contenedor y por último un minijuego donde facilitamos el aprendizaje al la hora de tirar los objetos en el contenedor correspondiente.

Esta publicada en [Google Play](https://play.google.com/store/apps/details?id=cat.copernic.xars.georecycle&gl=ES).

![Info GeoRecycle](https://gitlab.com/alexanderby54/alexander_garcia_portafolio/-/raw/master/images/info-georecycle.PNG)

****

### GExpenses

> Noviembre 2022 / Enero 2023

Este proyecto es una pagina web. El objetivo de GExpense, es organizar, distribuir y calcular gastos de las salidas con tus amigos, familiares, etc.

En este proyecto se ha utilizado como frontend:

    HTML 
    CSS 
    JavaScript

y como backend:

    PHP
    MySql
    Vagrant


![Landing GExpense](https://gitlab.com/alexanderby54/alexander_garcia_portafolio/-/raw/master/images/Landing.PNG)

****

### Crafty

> Enero 2023 / Mayo 2023

MarketPlace de productos, esta dirigida a pequeños artesanos que carecen de los recursos necesarios para expandir su negocio a nivel mundial. Para superar esta limitación, se plantea la creación de una página web que les permita exhibir y vender sus productos. Esta plataforma se presenta como una solución altamente viable, brindando la oportunidad a los artesanos de unirse y mostrar sus creaciones a un público más amplio. A través de este servicio, los pequeños artesanos podrán expandir sus horizontes más allá de su mercado local y conectar con potenciales clientes de todo el mundo.

Utilizamos un preprocesador de CSS: SASS

Framework utilizado:

    Laravel

En este proyecto se ha utilizado como frontend:

    HTML 
    CSS
    JavaScript

y como backend:

    PHP
    MySql
    Vagrant    

![Landing Crafty](https://gitlab.com/alexanderby54/alexander_garcia_portafolio/-/raw/master/images/landing-crafty.png)

****
### GymWRat

> Noviembre 2022 / Mayo 2023

El objetivo de GymWRat es el de ofrecer una plataforma intuitiva y agradable a todas las personas que quieren comenzar un cambio en su vida, ya sea en el ámbito del ejercicio o de la alimentación. Ofrecemos rutinas y una biblioteca de ejercicios muy amplia de forma totalmente gratuita para las personas que quieren iniciarse desde cero y también ofrecemos planes para las personas que quieren dar un paso más allá.

Utilizamos un preprocesador de CSS: SASS

Framework utilizado:

    Laravel

En este proyecto se ha utilizado como frontend:

    HTML 
    CSS
    JavaScript

y como backend:

    PHP
    MySql
    Vagrant    

![Landing Gymwrat](https://gitlab.com/alexanderby54/alexander_garcia_portafolio/-/raw/master/images/landing-gymwrat.PNG)


****
