---
title: GExpenses
dateMonthYear: Enero 2023
description: Este proyecto es una pagina web que tiene como objetivo organizar, distribuir y calcular gastos de las salidas con tus amigos, familiare, etc. En este proyecto hemos utilizado como FRONTEND (HTML, CSS, JavaScript), BACKEND (PHP, MySql, Vagrant).
topic: project
image: "https://gitlab.com/alexanderby54/alexander_garcia_portafolio/-/raw/master/images/Landing.PNG"
---
